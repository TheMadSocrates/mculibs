/*
 * ks0108b.h
 *
 *  Created on: Jul 12, 2011
 *      Author: Orlando Arias
 *     License: GPL v3
 *
 *    ks0108b
 *    Copyright (C) 2011  Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KS0108B_H_
#define KS0108B_H_

#include <inttypes.h>
#include <stdio.h>

#define ks_createStream() FDEV_SETUP_STREAM(ks_writeChar, NULL, _FDEV_SETUP_WRITE)

typedef enum _ks_command_set {
	KS_SET_Y = 0x40,		// or with address [0x00 - 0x3f]
	KS_SET_PAGE = 0xb8,		// or with page [0x00 - 0x07]
	KS_SET_Z = 0xc0,		// or with line [0x00 - 0x3f]
	KS_ON_OFF = 0x3e		// or with 0/1 [off/on]
} ks_command;

void ks_init();
void ks_sendCommand(ks_command c, uint8_t cs);
void ks_sendData(uint8_t byte, uint8_t cs);
int ks_writeChar(char c, FILE *unused);


#endif /* KS0108B_H_ */
