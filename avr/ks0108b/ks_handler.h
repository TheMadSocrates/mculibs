/*
 * ks_handler.h
 *
 *  Created on: Jul 12, 2011
 *      Author: Orlando Arias
 *     License: GPL v3
 *
 *    ks0108b
 *    Copyright (C) 2011  Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KS_HANDLER_H_
#define KS_HANDLER_H_

#include <avr/io.h>

#define CS1 0
#define CS2 1
#define RW 2
#define RS 3
#define RES 4
#define EN 5

#define _IOPORT A
#define _CTRLPORT C

#define DATADDR _ks_glue(DDR, _IOPORT)
#define CTRLDDR _ks_glue(DDR, _CTRLPORT)

#define DATAOUT _ks_glue(PORT, _IOPORT)
#define CTRLOUT _ks_glue(PORT, _CTRLPORT)
#define DATAIN _ks_glue(PIN, _IOPORT)

#define _ks_glue_(_a, _port) _a##_port
#define _ks_glue(_a, _port) _ks_glue_(_a, _port)



#endif /* KS_HANDLER_H_ */
