/*
 * hd44780.h
 *
 *  Created on: Dec 29, 2011
 *      Author: Orlando Arias
 *	   License: GPLv3
 *
 *    hd44780-4bit
 *    Copyright (C) 2011 Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HD44780_H_
#define HD44780_H_

#define hd_createStream() FDEV_SETUP_STREAM(hd_sendChar, 0, _FDEV_SETUP_WRITE)

void hd_init(void);
int hd_sendChar(char c, FILE *unused);

#endif /* HD44780_H_ */
