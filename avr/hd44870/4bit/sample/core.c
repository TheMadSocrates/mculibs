/*
 * core.c
 *
 *  Created on: May 18, 2012
 *      Author: Orlando Arias
 *	   License: GPLv3
 *
 *    hd44780-4bit-sample
 *    Copyright (C) 2012 Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <hd44780.h>

#include <avr/pgmspace.h>


FILE hd_stream = hd_createStream();		// Create a FILE buffer for the LCD.

void main(void) __attribute__((noreturn));

void main(void) {
	hd_init();							// LCD must be initialised before use.
	//stdout = &hd_stream;				// Assign pointer to buffer to stdout
										// for use with printf().

	fprintf_P(&hd_stream, PSTR("Hello, world!"));	// Alternatively:
										// 		fprintf_P(&hd_stream,
										//				PSTR("Hello, world!");
										// using this method, the assignment is
										// not required.

	while(1);
}
