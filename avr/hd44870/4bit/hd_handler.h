/*
 * hd_handler.h
 *
 *  Created on: Dec 29, 2011
 *      Author: Orlando Arias
 *	   License: GPLv3
 *
 *    hd44780-4bit
 *    Copyright (C) 2011 Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HD_HANDLER_H_
#define HD_HANDLER_H_

/// AVR specific
#include <avr/io.h>
#include <util/delay.h>

/*
 * Library configuration parameters:
 * 	_hd_port
 * 		Port the HD44780 LCD module will be connected to. Port must be at
 * 		least 7 bits wide, 8 bits if bell support is desired. Port must also
 * 		be bidirectional.
 * 	_rows
 * 		Number of text rows on the LCD.
 * 	_cols
 * 		Number of text columns on the LCD.
 * 	RS, EN, RW
 * 		Register Select, Enable and Read/~Write signals,
 * 		respectively. Must be defined as bit number in I/O port.
 * 	BELL
 * 		Optional terminal bell for '\a'. To compile with bell support
 * 		pass -D_has_bell as a compiler argument.
 * 	BELL_LEN
 * 		Length in milliseconds of the bell. See manual for _delay_ms()
 * 		for limits of operation.
 * 	A00 ROM notes
 * 		Flag for the A00 ROM. The A00 ROM contains alternate characters for
 * 		'g', 'j', 'p', 'q' and 'y'. To properly show these characters, the
 * 		matrix on the LCD must be 5x10. If you have an A00 ROM on your LCD and
 * 		you wish to use the alternate characters, pass -D_a00_rom as a compiler
 * 		argument.
 */

#define _hd_port D
#define _rows 4
#define _cols 20

#define RS 4
#define RW 5
#define EN 6
#ifdef _has_bell
#	define BELL 7
#	define BELL_LEN 50
#endif

/*
 *  Internal macros and library functions.
 *
 *  Porting guidelines:
 *		Macros for HDDDR, HDPORT and HDPIN must be defined.
 *		Macro for a delay function must be defined. Delay is expected in
 *		milliseconds.
 *		Busy flag bit must be defined.
 *		A _bv(n) macro must be defined. _bv stands for bit value and must
 *		return (1 << (n)).
 */

#define BF 3
#define HDDDR _hd_glue(DDR, _hd_port)
#define HDPORT _hd_glue(PORT, _hd_port)
#define HDPIN _hd_glue(PIN, _hd_port)

#define _hd_glue_(_a, _port) _a##_port
#define _hd_glue(_a, _port) _hd_glue_(_a, _port)

#define _delay_function(x) _delay_ms(x)
#define _bv(n) _BV(n)

#endif /* HD_HANDLER_H_ */
