/*
 * font3x5.h
 *
 *  Created on: Jul 11, 2011
 *      Author: Orlando Arias
 *     License: GPL v3
 *
 *    ks0108b
 *    Copyright (C) 2011  Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FONT3X5_H_
#define FONT3X5_H_

#include <avr/pgmspace.h>

#ifndef FONT_INCLUDE_
#	define FONT_INCLUDE_
#	define FONT_H 5
#	define FONT_W 3
#	define TAB_CONST (4 * 8/(FONT_W + 1))
#else
#	error Can only include one font file.
#endif

const uint8_t font[] PROGMEM = {0x00, 0x5c, 0x00,		0x0c, 0x00, 0x0c,		0x7c, 0x28, 0x7c,
							0x28, 0x7c, 0x14,		0x24, 0x10, 0x48,		0x28, 0x54, 0x60,
							0x00, 0x0c, 0x00,		0x38, 0x44, 0x00,		0x00, 0x44, 0x38,
							0x28, 0x10, 0x28,		0x10, 0x38, 0x10,		0x40, 0x20, 0x00,
							0x10, 0x10, 0x10,		0x00, 0x40, 0x00,		0x60, 0x10, 0x0c,
							0x38, 0x54, 0x38,		0x48, 0x7c, 0x40,		0x48, 0x64, 0x58,
							0x44, 0x54, 0x28,		0x1c, 0x10, 0x7c,		0x5c, 0x54, 0x24,
							0x38, 0x54, 0x20,		0x04, 0x64, 0x1c,		0x28, 0x54, 0x28,
							0x08, 0x54, 0x38,		0x00, 0x28, 0x00,		0x40, 0x28, 0x00,
							0x10, 0x28, 0x44,		0x28, 0x28, 0x28,		0x44, 0x28, 0x10,
							0x54, 0x14, 0x08,		0x30, 0x48, 0x50,		0x78, 0x14, 0x78,
							0x7c, 0x54, 0x28,		0x38, 0x44, 0x44,		0x7c, 0x44, 0x38,
							0x7c, 0x54, 0x44,		0x7c, 0x14, 0x04,		0x38, 0x44, 0x64,
							0x7c, 0x10, 0x7c,		0x44, 0x7c, 0x44,		0x44, 0x44, 0x3c,
							0x7c, 0x10, 0x6c,		0x7c, 0x40, 0x40,		0x7c, 0x08, 0x7c,
							0x7c, 0x04, 0x7c,		0x38, 0x44, 0x38,		0x7c, 0x14, 0x08,
							0x38, 0x44, 0x78,		0x7c, 0x14, 0x68,		0x48, 0x54, 0x24,
							0x04, 0x7c, 0x04,		0x7c, 0x40, 0x7c,		0x3c, 0x40, 0x3c,
							0x7c, 0x20, 0x7c,		0x6c, 0x10, 0x6c,		0x0c, 0x70, 0x0c,
							0x64, 0x54, 0x4c,		0x7c, 0x44, 0x00,		0x0c, 0x10, 0x60,
							0x00, 0x44, 0x7c,		0x08, 0x04, 0x08,		0x40, 0x40, 0x40,
							0x04, 0x08, 0x00,		0x68, 0x68, 0x70,		0x7c, 0x50, 0x20,
							0x30, 0x48, 0x48,		0x20, 0x50, 0x7c,		0x30, 0x58, 0x58,
							0x78, 0x24, 0x08,		0x48, 0x54, 0x38,		0x7c, 0x10, 0x60,
							0x50, 0x74, 0x40,		0x40, 0x50, 0x34,		0x7c, 0x20, 0x50,
							0x44, 0x7c, 0x40,		0x78, 0x10, 0x78,		0x78, 0x08, 0x70,
							0x30, 0x48, 0x30,		0x78, 0x28, 0x10,		0x08, 0x54, 0x78,
							0x78, 0x10, 0x08,		0x50, 0x58, 0x28,		0x08, 0x3c, 0x48,
							0x38, 0x40, 0x78,		0x38, 0x40, 0x38,		0x78, 0x20, 0x78,
							0x50, 0x20, 0x50,		0x4c, 0x50, 0x3c,		0x68, 0x58, 0x48,
							0x10, 0x6c, 0x44,		0x00, 0x7c, 0x00,		0x44, 0x6c, 0x10,
							0x30, 0x10, 0x18,		0x08, 0x7c, 0x08,		0x20, 0x7c, 0x20,
							0x10, 0x38, 0x7c,		0x7c, 0x38, 0x10 };

#endif /* FONT3X5_H_ */
