/*
 * hd_common.h
 *
 *  Created on: Jun 2, 2012
 *      Author: Orlando Arias
 *	   License: GPLv3
 *
 *    hd44780-4bit
 *    Copyright (C) 2012 Orlando Arias
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HD_COMMON_H_
#define HD_COMMON_H_

#include <inttypes.h>

#include "hd_handler.h"

#define _wait_for_busy_flag() while(_hd_bf())

static const uint8_t _hd_map[] = {0x00, 0x40
#	if _rows == 4
		, 0x14, 0x54
#	endif
		};

typedef enum _command_set {
	CLEAR = 0x01,						// Clear screen and set cursor to 0x00
	MOVE_HOME = 0x02,					// Set cursor to 0x00
	EM_DECREMENT_OFF = 0x04,			// Move cursor left after write
										// to DDRAM
	EM_DECREMENT_ON = 0x05,				// Move text right after write
										// to DDRAM
	EM_INCREMENT_OFF = 0x06,			// Move cursor left after write
										// to DDRAM
	EM_INCREMENT_ON = 0x07,				// Move text right after write
										// to DDRAM
	BLANK_DISPLAY = 0x08,				// Display on/off
	CURSOR_INVISIBLE = 0x0C,			// Cursor is not visible
	CURSOR_VISIBLE_BLOCK = 0x0D,		// Cursor visible as alternating block
	CURSOR_VISIBLE_UNDERLINE = 0x0E,	// Cursor visible as underline
	CURSOR_VISIBLE_ALT = 0x0F,			// Cursor visible as blinking
										// block/underline
	MOVE_LEFT = 0x10,					// Move cursor left
	MOVE_RIGHT = 0x14,					// Move cursor right
	SCROLL_LEFT = 0x18,					// Scroll DDRAM left
	SCROLL_RIGHT = 0x1E,				// Scroll DDRAM right
	SET_CGRAM = 0x40,					// Sets cursor position on CGRAM
										// (or pos << 3 with function)
	SET_DDRAM = 0x80					// Sets cursor position on DDRAM
										// (or with function)
} command;

static uint8_t _row;
static uint8_t _col;

void _hd_sendRaw(char c, uint8_t _rs);
void _hd_strobe(void);
uint8_t _hd_bf(void);

#endif /* HD_COMMON_H_ */
